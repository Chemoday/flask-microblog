#.:/usr/bin/env python
from flask import Flask
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_moment import Moment


from test_app.config import config

#db modules
import peewee
from flask_peewee.db import Database

from peewee_moves import DatabaseManager

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'


bootstrap = Bootstrap()
moment = Moment()
db = Database()  #Using to configure db in run-tine and initialize with delay
#db_manager = DatabaseManager(Config.DATABASE)

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    login_manager.init_app(app)
    db.init_app(app)
    bootstrap.init_app(app)
    moment.init_app(app)



    from .main import main_bp as main_blueprint

    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint

    app.register_blueprint(auth_blueprint)





    return app