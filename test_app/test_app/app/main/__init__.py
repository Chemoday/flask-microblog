from flask import Blueprint
from ..models import Permission
main_bp = Blueprint('main', __name__)

from . import views, errors

@main_bp.context_processor
def inject_permissions():
    return dict(Permission=Permission)