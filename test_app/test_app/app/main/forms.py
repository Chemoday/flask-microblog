from flask_wtf import FlaskForm
from wtforms import *
from wtforms.validators import *
from ..models import *

class NameForm(FlaskForm):
    name = StringField('What is your name?', validators=[DataRequired()])
    submit = SubmitField('Submit')


class EditProfileForm(FlaskForm):
    name = StringField('Real name', validators=[Length(0,64)])
    location = StringField('Location', validators=[Length(0,64)])
    about_me = TextAreaField('About me')
    submit = SubmitField('Submit')

class EditProfileAdminForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Length(5,64), Email() ])
    username = StringField('Username', validators=[DataRequired(), Length(1,64)])
    role = SelectField('Role', coerce=int)
    name = StringField('Real name', validators=[Length(0,64)])
    location = StringField('Location', validators=[Length(0,64)])
    about_me = TextAreaField('About Me')
    submit = SubmitField('Submit')

    def __init__(self, user, *args, **kwargs):
        super(EditProfileAdminForm, self).__init__(*args, **kwargs)
        roles = Role.select()
        self.role.choices = [(role.id, role.name)
                             for role in roles]
        self.user = user

    def validate_email(self, field):
        if field.data != self.user.email:
            q = User.select().where(User.email == field.data)
            if q.exists():
                raise ValidationError('Email alraedy registered')

    def validate_username(self, field):
        if field.data != self.user.username:
            q = User.select().where(User.username == field.data)
            if q.exists():
                raise ValidationError('Username already in use')


class PostForm(FlaskForm):
    body = TextAreaField("Tell something...", validators=[DataRequired()])
    submit = SubmitField('Submit')

