from flask import render_template, session, \
    redirect, url_for, current_app, flash, request, make_response
from flask_login import login_required, current_user
from flask_peewee.utils import object_list
from .. import db

from . import main_bp
from .forms import *

from ..models import Post, User

from ..decorators import *

@main_bp.route('/', methods=['GET', 'POST'])
def index():

    show_followed = False
    if current_user.is_authenticated:
        show_followed = bool(request.cookies.get('show_followed', ''))

    if show_followed:
        posts = Post.get_recommended(user_id=current_user.id)
    else:
        posts = Post.select().order_by(Post.timestamp.desc())

    form = PostForm()

    if current_user.can(Permission.WRITE_ARTICLES) and \
        form.validate_on_submit():
        post = Post.create(body=form.body.data, author_id=current_user._get_current_object())
        return redirect(url_for('.index'))



    return object_list('index.html',qr=posts,
                       var_name='posts', paginate_by=current_app.config['POSTS_PER_PAGE'],
                       form=form, show_followed=show_followed) #For easier pagination


@main_bp.route('/user/<username>')
def user_profile(username):
    q = User.select().where(User.username == username)
    if q.exists():
        user = User.get(User.username == username)
        posts = user.posts.select().order_by(Post.timestamp.desc())

        return object_list('user.html', qr=posts, var_name='posts',
                           paginate_by=current_app.config['POSTS_PER_PAGE'], user=user)
    else:
        return render_template('404.html')

@main_bp.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.name = form.name.data
        current_user.location = form.location.data
        current_user.about_me = form.about_me.data
        current_user.save(only=[User.name,
                                User.location,
                                User.about_me])
        flash('Your profile has been updated.')
        return redirect(url_for('main.user_profile', username=current_user.username))

    form.name.data = current_user.name
    form.location.data = current_user.location
    form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', form=form)

#@admin_required
@main_bp.route('/edit-profile/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_profile_admin(id):
    user = User.get(User.id == id)
    form = EditProfileAdminForm(user=user)
    if form.validate_on_submit():
        user.email = form.email.data
        user.username = form.username.data
        user.role_id = Role.select().where(Role.id == form.role.data)
        user.name = form.name.data
        user.location = form.location.data
        user.about_me = form.about_me.data
        user.save(only=[User.email, User.username,
                        User.role_id, User.name,
                        User.location, User.about_me])
        flash('The profile has been updated.')
        return redirect(url_for('.user_profile', username=user.username))
    form.email.data = user.email
    form.username.data = user.username
    form.role.data = user.role_id
    form.name.data = user.name
    form.location.data = user.location
    form.about_me.data = user.about_me
    return render_template('edit_profile.html', form=form, user=user)


@main_bp.route('/post/<int:id>', methods=['GET', 'POST'])
def post(id):
    q = Post.select().where(Post.id == id)
    if q.exists():
        post = Post.get(Post.id == id)
        form = PostForm()
        comments = Commentary.select().where(Commentary.post_id == post.id)

        if current_user.can(Permission.COMMENT) and \
                form.validate_on_submit():
                Commentary.create(author_id=current_user.id, post_id=post.id, body=form.body.data)
                return redirect(request.referrer)

        return object_list('post.html', qr=comments,
                       var_name='comments', paginate_by=current_app.config['POSTS_PER_PAGE'], post=post, form=form)  # For easier pagination
    else:
        return url_for('.404.html')

@main_bp.route('/comment/up_vote/<comment_id>')
def up_vote_commentary(comment_id):
    if Commentary.already_voted(user_id=current_user.id, comment_id=comment_id):
        flash('Comment is already voted')
        return redirect(request.referrer)
    else:
        Commentary_vote.create(comment_id=comment_id, voter_id=current_user.id)
        Commentary.update(rating=Commentary.rating + 1).where(Commentary.id == comment_id).execute()
        return redirect(request.referrer)

@main_bp.route('/comment/down_vote/<comment_id>')
def down_vote_commentary(comment_id):
    if Commentary.already_voted(user_id=current_user.id, comment_id=comment_id):
        flash('Comment is already voted')
        return redirect(request.referrer)
    else:
        Commentary_vote.create(comment_id=comment_id, voter_id=current_user.id)
        Commentary.update(rating=Commentary.rating - 1).where(Commentary.id == comment_id).execute()
        return redirect(request.referrer)


@main_bp.route('/post/recommended')
def post_recommended():
    posts = Post.get_recommended(user_id=current_user.id)
    return object_list('posts_recommended.html', qr=posts,
                       var_name='posts', paginate_by=current_app.config['POSTS_PER_PAGE'])  # For easier pagination


@main_bp.route('/follow/<username>/')
@login_required
#@permission_required(Permission.FOLLOW)
def follow(username):
    user = User.select().where(User.username == username)
    if not user.exists():
        flash('Invalid user')
        return redirect(request.referrer)

    user = User.get(User.username == username)
    if current_user.is_following(user=user):
        flash('You are already following this user.')
        return redirect(url_for('.user_profile', username=user.username))


    print("Trying to follow")
    current_user.follow(user=user)
    flash('You are now following : {0}'.format(user.username))
    return redirect(request.referrer)


@main_bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    if User.select().where(User.username == username).exists():
        user = User.get(User.username == username)
        if current_user.unfollow(user):
            flash("You Unfollowed {0}".format(user.username))
    else:
        flash("Invalid user to follow")

    return redirect(request.referrer)

@main_bp.route('/followers/<username>')
def followers(username):
    user = User.select().where(User.username == username).first()

    if not user:
        flash('Invalid user')
        return redirect(request.referrer)

    user_followers = user.followers()

    return object_list('followers_user.html',qr=user_followers,
                       var_name='followers', paginate_by=current_app.config['POSTS_PER_PAGE']) #For easier pagination

@main_bp.route('/followed/<username>')
def followed_by_user(username):
    user = User.select().where(User.username == username).first()

    if not user:
        flash('Invalid user')
        return redirect(request.referrer)

    followed_by_user = user.following()

    return object_list('followed_by_user.html',qr=followed_by_user,
                       var_name='followed_by_user', paginate_by=current_app.config['POSTS_PER_PAGE']) #For easier pagination

@main_bp.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@main_bp.route('/all')
@login_required
def show_all():
        resp = make_response(redirect(url_for('.index')))
        resp.set_cookie('show_followed', '', max_age=30*24*60*60)
        return resp

@main_bp.route('/followed_posts')
@login_required
def show_followed():
    resp = make_response(redirect(url_for('.index')))
    resp.set_cookie('show_followed', '1', max_age=30*24*60*60)
    return resp


