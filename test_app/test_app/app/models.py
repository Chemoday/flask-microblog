from flask_login import UserMixin, AnonymousUserMixin

from . import login_manager
from . import db

import random
from test_app.utils.dbStruct import PostStruct, UserStruct

from peewee import *
from playhouse.fields import ManyToManyField
from werkzeug.security import generate_password_hash, check_password_hash

import datetime



class Permission:
    FOLLOW = 0x01
    COMMENT = 0x02
    WRITE_ARTICLES = 0x04
    MODERATE_COMMENTS = 0x08
    ADMINISTER = 0x80

class Role(db.Model):
    id = PrimaryKeyField()
    name = CharField(max_length=64, unique=True)
    default = BooleanField(default=False, index=True)
    permissions = IntegerField(null=True)


    class Meta:
        db_table = 'roles'

    @staticmethod
    def insert_roles():
        roles = {
            'User': (Permission.FOLLOW |
                     Permission.COMMENT |
                     Permission.WRITE_ARTICLES, True),
            'Moderator': (Permission.FOLLOW |
                          Permission.COMMENT |
                          Permission.WRITE_ARTICLES |
                          Permission.MODERATE_COMMENTS, False),
            'Administrator': (0xff, False)
        }
        try:
            for r in roles:
                role = Role.select().where(Role.name == r).first()
                if role is None:
                    role = Role.create(name=r)
                role.permissions = roles[r][0]
                role.default = roles[r][1]
                role.save()
        except Exception as e:
            print(e)
            return



class User(UserMixin, db.Model):
    id = PrimaryKeyField()
    email = CharField(max_length=64, unique=True, index=True)
    username = CharField(max_length=64, unique=True,
                                index=True)
    role_id = ForeignKeyField(Role, related_name='role', to_field='id', default=3)
    password_hash = CharField(max_length=128)
    name = CharField(max_length=64, null=True)

    location = CharField(max_length=64, null=True)
    about_me = TextField(default=" ")
    member_since = DateTimeField(default=datetime.datetime.utcnow())
    last_seen = DateTimeField(default=datetime.datetime.utcnow())

    def can(self, permissions):
        return self.role_id is not None and \
               (self.role_id.permissions & permissions) == permissions

    def is_admin(self):
        return self.can(Permission.ADMINISTER)

    def ping(self):
        self.last_seen = datetime.datetime.utcnow()
        self.save()

    @property
    def total_followers(self):
        return User.select().join(
            Follow, on=Follow.follower_id
        ).where(Follow.followed_id==self).count()

    @property
    def total_followed(self):
        return User.select().join(
            Follow, on=Follow.followed_id
        ).where(Follow.follower_id == self).count()

    def follow(self, user):
        if not self.is_following(user):
            Follow.create(follower_id=self,followed_id=user)



    def is_following(self, user):
        return Follow.select().where(
            Follow.follower_id == self,
            Follow.followed_id == user
        ).exists()

    def unfollow(self, user):
        if self.is_following(user):
            Follow.delete().where(Follow.follower_id == self,
                                   Follow.followed_id == user).execute()
            return True

    def following(self):
        return User.select().join(
            Follow, on=Follow.followed_id
        ).where(Follow.follower_id == self).order_by(User.id)

    def followers(self):
        return User.select().join(
            Follow, on=Follow.follower_id
        ).where(Follow.followed_id==self).order_by(User.id)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)



    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)




    @staticmethod
    def insert_users():

        admin_id = Role.get(Role.name == 'Administrator')
        mod_id = Role.get(Role.name == 'Moderator')
        user_id = Role.get(Role.name == 'User')
        admin_user = User.create(email='admin@admin.com', username='Administrator',role_id=admin_id,
                                 password='admin')
        mod_user = User.create(email='mod@mod.com', username='Moderator',role_id=mod_id,
                               password='mod')
        default_user = User.create(email='user@user.com', username='Default User',role_id=user_id,
                                   password='user')

        with db.database.atomic():
            source_data = []
            for i in range(100):
                user_struct = UserStruct()
                source_data.append(user_struct.struct)
            User.insert_many(source_data).execute()



    def __repr__(self):
        return '<User {0}>'.format(self.username)

    class Meta:
        db_table = 'users'


class AnonymousUser(AnonymousUserMixin):
    def can(self, permissions):
        return False

    def is_admin(self):
        return False
    @property
    def is_authenticated(self):
        return False



class Post(db.Model):
    id = IntegerField(primary_key=True)
    body = TextField()
    timestamp = DateTimeField(index=True, default=datetime.datetime.now)
    author_id = ForeignKeyField(User, related_name='posts', to_field='id')

    class Meta:
        db_table = 'posts'

    @staticmethod
    def get_recommended(user_id):
        return Post.select().join(
            Follow, on=(Follow.followed_id == Post.author_id)).where(
            Follow.follower_id == user_id).order_by(Post.timestamp.desc())




    @staticmethod
    def insert_posts():
        user_list = User.select()

        with db.database.atomic():
            source_data = []
            for user in user_list:
                for i in range(random.randint(1,5)):
                    post = PostStruct(user)
                    source_data.append(post.struct)
            Post.insert_many(source_data).execute()



login_manager.anonymous_user = AnonymousUser

class Commentary(db.Model):
    id = PrimaryKeyField()
    author_id = ForeignKeyField(User, to_field='id')
    post_id = ForeignKeyField(Post, to_field='id')
    body = TextField()
    rating = IntegerField(default=0)
    timestamp = DateTimeField(index=True, default=datetime.datetime.now)

    @staticmethod
    def already_voted(user_id, comment_id):
        if Commentary_vote.select().where(Commentary_vote.comment_id == comment_id,
                                          Commentary_vote.voter_id == user_id).exists():
            return True
        else:
            return False

    @property
    def total_votes(self):
        return Commentary.select(Commentary.rating).where(Commentary.id == self.id)



class Commentary_vote(db.Model):
    voter_id = ForeignKeyField(User, to_field='id')
    comment_id = ForeignKeyField(Commentary, to_field='id')





@login_manager.user_loader
def load_user(user_id):
    return User.get(id=user_id)


class Follow(db.Model):
    follower_id = ForeignKeyField(User, related_name='follower')
    followed_id = ForeignKeyField(User, related_name='followed')
    timestamp = DateTimeField(default=datetime.datetime.utcnow())

    class Meta:
        indexes = (
            (('follower_id_id', 'followed_id_id'), True)
        )
        db_table = 'follows'