#.:/usr/bin/env python
import os
from app import create_app, db
from app.models import User, Role, Post, Follow, Commentary, Commentary_vote

from flask_script import Manager, Shell
#from flask_migrate import Migrate, MigrateCommand

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
#migrate = Migrate(app, db)

models_list = [User, Role, Post, Follow, Commentary, Commentary_vote]

def make_shell_context():
    return dict(app=app, db=db, User=User, Role=Role)

@manager.command
def test():
    """Run the unit tests."""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)

@manager.command
def generate_db_tables():
    db_lite = connect_db()
    for model in models_list:
        db_lite.create_table(model, safe=True)
    print("Db tables created")

def connect_db():
    from peewee import SqliteDatabase
    db_lite = SqliteDatabase(app.config['DATABASE']['name'])
    print(db_lite)
    return db_lite

@manager.command
def fill_tables_data():
    Role.insert_roles()
    User.insert_users()
    Post.insert_posts()
    print('Db data was filled')

@manager.command
def fill_database():
    db_fill = connect_db()
    fill_tables_data(db_fill)

@manager.command
def drop_db_tables(model=None):
    db = connect_db()
    if model:
        db.drop_table(model, fail_silently=True)
    for model in models_list:
        db.drop_table(model, fail_silently=True)


manager.add_command("shell", Shell(make_context=make_shell_context))

if __name__ == '__main__':
    manager.run()