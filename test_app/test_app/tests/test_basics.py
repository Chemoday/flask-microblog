import unittest
from flask import current_app
from test_app.app import create_app, db
from .test_commentary import CommentaryModelTestCase

class BasicsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('default')
        self.app_context = self.app.app_context()
        self.app_context.push()
        print('here')


    def tearDown(self):
        self.app_context.pop()

    def test_app_exists(self):
        CommentaryModelTestCase()
        self.assertFalse(current_app is None)

