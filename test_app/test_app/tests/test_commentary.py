import unittest
from flask import current_app
from test_app.app import create_app, db


class CommentaryModelTestCase(unittest.TestCase):
    def setUp(self):
        print('here')
        self.app = create_app('default')
        self.app_context = self.app.app_context()
        self.app_context.push()


    def tearDown(self):
        self.app_context.pop()

    def addLike(self):
        self.app.post('/login', data=dict(email='admin@admin.com',password='admin'), #login
                      follow_redirects=True)

        #self.app.get('/post/1', follow_redirects=True)

        with self.app.test_request_context('/post/1'):
            self.assertFalse(self.app.request.path == '/post/1')