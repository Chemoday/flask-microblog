from faker import Faker

class BaseStruct(object):
    fake = Faker()

class UserStruct(BaseStruct):
    def __init__(self):

        self.struct = {
            "email": self.fake.email(),
            "username": self.fake.user_name(),
            "password_hash": 'pbkdf2:sha256:50000$M62abUvY$af1980c16cb7c22d66b7a1a850bc290c4d6910734421a2393de423ad75758e78',
            'name': self.fake.name(),
            'location': self.fake.street_address(),
            'about_me': self.fake.sentence()

        }

class PostStruct(BaseStruct):
    def __init__(self, user):
        self.struct = {
                        "body": self.fake.text(),
                        "author_id": user.id
        }